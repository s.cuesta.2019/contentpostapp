import socket

class WebApp:
    def parse(self, request):
        return None

    def process(self, parsedRequest):
        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.hostname, self.port))
        self.socket.listen(5)

        print(f"Web server running on http://{self.hostname}:{self.port}")

    def start(self):
        try:
            while True:
                print(f"Waiting for connections on port {self.port}")
                client_socket, client_address = self.socket.accept()
                print(f"Connection from {client_address}")

                request_data = client_socket.recv(1024).decode('utf-8')
                print(f"Request received:\n{request_data}")

                parsed_request = self.parse(request_data)
                http_response = self.process(parsed_request)

                response = "HTTP/1.1 {}\r\n\r\n{}".format(http_response[0], http_response[1])
                client_socket.sendall(response.encode('utf-8'))
                client_socket.close()

        except KeyboardInterrupt:
            print("\nShutting down the server")
        finally:
            self.socket.close()

if __name__ == "__main__":
    app = WebApp("localhost", 1234)
    app.start()